var webpack = require('webpack');

module.exports = {
  entry: './app/index.js',

  output: {
    path: './assets',
    filename: 'bundle.js'
  },

  resolve: {
    extensions: ['', '.js']
  },

  debug: true,
  devtool: 'cheap-module-eval-source-map',

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react','es2015']
        }
      }
    ]
  }
};