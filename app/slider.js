var React = require('react');

'strict mode'

class Slider extends React.Component{
    render(){
        return (
                <div>
                    <input ref="inp" type="range" onChange={this.props.update} />
                </div>
            );
    }
}

export default Slider;