/// <reference path="../typings/tsd.d.ts" />

var React = require('react');
var ReactDOM = require('react-dom');
import Widget from './widget.js';
import Slider from './slider.js';

//const App = () => <h1>Arrow method component - can`t have a state</h1>

class App extends React.Component{

    constructor(){
        super();
        this.state = {
            s: 'this is state variable',
            red: 0,
            blue: 0
        }
    }

    update(e){
        //console.log(this);
        this.setState({s: e.target.value});
    }

    updateSlider(e){
        this.setState({
             red: ReactDOM.findDOMNode(this.refs.red.refs.inp).value,
             blue:ReactDOM.findDOMNode(this.refs.blue.refs.inp).value
            });
    }

    render() {
        let txt = this.props.txt;
        let nb = this.props.nb;
        return (
            <div>
                <Widget txt={this.txt} s={this.state.s} update={this.update.bind(this)}/>
                <Widget txt={this.txt} s={this.state.s} update={this.update.bind(this)}/>
                <hr/>
                <div>              
                    <Slider ref="red" update={this.updateSlider.bind(this)}/>
                    {this.state.red}
                    <Slider ref="blue" update={this.updateSlider.bind(this)}/>
                    {this.state.blue}
                </div>
            </div>
            );
    }
}

App.propTypes  = {
    txt: React.PropTypes.string,
    nb: React.PropTypes.number.isRequired
}; 

App.defaultProps = {
    txt: 'default txt'
}

export default App;

