var React = require('react');

'strict mode'

class Widget extends React.Component{
    render(){
        return (
            <div>
                <h1>Hello, world changed</h1>
                <b>{this.props.txt}</b>
                <p>{this.props.s}</p>
                <input type="text" onChange={this.props.update} value={this.props.s}/>
            </div>
            );
    }
}

export default Widget;